import React from 'react'
import ReactDOM from 'react-dom'
import Main from './component/Main/Main.jsx'

ReactDOM.render(<Main />, document.getElementById('root'))
